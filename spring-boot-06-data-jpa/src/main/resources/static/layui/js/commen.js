// 创建列表
var baseTable = null;
var tableProperty = {
    elem: '#',
    height: 'full-172',
    align:'left',
    url:'',
    cols: [[]],
    id:'tableReload',
    where:null,
    page: {
        theme:'#479de6'
    },
    done:null
};

var tableObj = {
    table: null,
    element:null,
    upload:null
};

function createTable(){
    baseTable =  tableObj.table.render({
        elem: tableProperty.elem,
        height: tableProperty.height,
        align:tableProperty.align,
        url:tableProperty.url,
        cols: tableProperty.cols,
        id:tableProperty.id,
        page: tableProperty.page,
        where:tableProperty.where,
        // limits: limits,
        // limit: pageSize ,//每页默认显示的数量
        done:function(res, curr, count){
            if(null != tableProperty.done){
                tableProperty.done(tableObj,res, curr, count)
            }
        }
    });
}

function createLayUiUse(arr,fun){
    layui.use(arr,fun);
}



var creatZTree = {
    id:"id",
    pId:"pid",
    url:"",
    showTreeId:"showTreeId",
    hiddenTreeId:"hiddenTreeId",
    zTreeOnClick:function(event, treeId, treeNode){}
}

function InitZStree(){
    var zTree;
    var setting = {
        data : {
            simpleData : {
                enable : true,
                idKey : creatZTree.id,
                pIdKey : creatZTree.pId,
                rootPId : null
            }
        },
        check: {
            autoCheckTrigger: true,
            chkStyle: "checkbox",
            chkboxType: { "Y": "s", "N": "s" }
        },
        callback: {
            onClick: creatZTree.zTreeOnClick
        }
    };
    $.getJSON(creatZTree.url,function(result){
        $.fn.zTree.init(
            $("#"+creatZTree.showTreeId),
            setting,
            result
        );
        $.fn.zTree.init(
            $("#"+creatZTree.hiddenTreeId),
            setting,
            result
        );

    });
}



function createSelect(date,pid,childId){
    $.post(pname+'/Basedata/getBasedataTypeTree',date,
        function(data,status){
            var options = '<option value="" selected = selected>--'+'请选择'+'--</option>';
            $.each(data, function(i, item) {
                options += ('<option value="'+item.id+'">'+item.value+'</option>');
            })
            $("#"+INTEREST_SELECT_ID+"_"+NUMBER_OF_BARS).append(options);
            tableObj.form.render('select');
    });

    tableObj.form.on('select(safetyPatrol)', function (data) {
        var date = {
            code:'safetyPatrol_child',
            pid:data.value
        }
        var pid = data.elem.id;
        var index = pid.replace("INTEREST_SELECT_ID_","")
        $.post(pname+'/Basedata/getBasedataTypeTree',date,
            function(data,status){
                var options = "";
                $.each(data, function(i, item) {
                    options += ('<option value="'+item.id+'">'+item.value+'</option>');
                })

                $("#"+INTEREST_SELECT_ID_CHILD+"_"+index).empty();
                $("#"+INTEREST_SELECT_ID_CHILD+"_"+index).append(options);
                tableObj.form.render('select');
            });
    });
}