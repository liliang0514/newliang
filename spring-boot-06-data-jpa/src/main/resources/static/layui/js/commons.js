$(function(){
    //提示框最小化
	$("#minAddId").click(function(){
		$("#infoTip").css({
			display: 'none'
		})
		
		$("#infoMin").css('display','block').click(function(){
			$("#infoTip").css('display','block');
			$("#infoMin").css('display','none');
		})
	})
	$("#minAddId2").click(function(){
		$("#infoTip2").css({
			display: 'none'
		})
		
		$("#infoMin2").css('display','block').click(function(){
			$("#infoTip2").css('display','block');
			$("#infoMin2").css('display','none');
		})
	})
	//提示框关闭按钮
	$("#closeAddId").click(function(){
		$("#infoTip").css('display','none');
	})
	$("#closeAddId2").click(function(){
		$("#infoTip2").css('display','none');
	})
	
	//初始化消息弹框
	$(".infoTip").css('display','none');
	$(".infoMin").css('display','none');
	if(orgType==1){//学校消息弹框
		$("#infoBtn2").css("display","block");
		schoolMessage();
		schoolNotify();
		schoolMessageUp();//右上角
		var t1 = window.setInterval("schoolMessage()",1000*60); 
		var t2 = window.setInterval("schoolNotify()",1000*60); 
		window.setInterval("schoolMessageUp()",1000*60); 
	}else{//教委消息提醒
		if(uDuty=='巡视员'||uDuty=='安全员'||uDuty=='巡查员'){//巡视员
			$("#infoBtn3").css("display","block");
			PacMessage();
			window.setInterval("PacMessage()",1000*60); 
		}else{//教委
			$("#workId").css("display","block");
			$("#monitorId").css("display","block");
			$("#infoBtn").css("display","block");
			eduMessage();
			var t3 = window.setInterval("eduMessage()",1000*60); 
		}
		
	}
	
	 // 教委消息按钮
    $("#infoBtn").click(function(){
    	var block =  $("#eduBoxId").css("display");
    	if(block=="block"){
    		$("#eduBoxId").css("display","none");
    	}else{
    		$("#eduBoxId").css("display","block");
    	}
    })
    //学校消息按钮
    $("#infoBtn2").click(function(){
    	var block =  $("#schoolBoxId").css("display");
    	if(block=="block"){
    		$("#schoolBoxId").css("display","none");
    	}else{
    		$("#schoolBoxId").css("display","block");
    	}
    })
    //巡视员消息按钮
    $("#infoBtn3").click(function(){
    	var block =  $("#pacBoxId").css("display");
    	if(block=="block"){
    		$("#pacBoxId").css("display","none");
    	}else{
    		$("#pacBoxId").css("display","block");
    	}
    })
    $(document).click(function(e){//点击空白消失
    	var _con3 = $('#infoBtn3'); 
    	var _con2 = $('#infoBtn2'); 
    	var _con = $('#infoBtn'); 
        if(!_con3.is(e.target) &&
        		!_con2.is(e.target) &&
        		!_con.is(e.target) &&_con.has(e.target).length === 0){ 
        	$(".infoBox").css("display","none");
        }
	});
    $("#infoCloseBtn,#infoCloseBtn2").click(function(){
      $(".infoBox").css("display","none");
    })


    $(".cancel").click(function(){
        layer.closeAll();
    });
})
//巡视员消息提醒
function PacMessage(){
	var url = pname +"/ZeroReport/getPatrolMsg";
	$.post(url,function(res){
		/*$("#pacSubmitId").html(res.pacSubmitId);*/
		$("#pacErrorId").html(res.pacErrorId);
		$("#pacReId").html(res.pacReId);
		$("#pacReErrorId").html(res.pacReErrorId);
		$("#loginTimeSpanId3").html(res.time.substring(0,19));
		var num=res.pacErrorId+res.pacReId+res.pacReErrorId;
		if(num=="000"){
			$("#classRedSpanId3").removeClass("layui-badge-dot")
		}else{
			$("#classRedSpanId3").addClass("layui-badge-dot")
		}
	});
	var url2 = pname +"/ZeroReport/getPatrolInfo";
	$.post(url2,function(res){
		var list = res.list;
		for(var i=0;i<list.length;i++){
			var div = $("<div/>").prop("class","infoTip")
				var div2 = $("<div/>").prop("class","infoTip-top")
					var em = $("<em/>").attr("class","icon-tip")
					var p = $("<i/>").attr("class","infoTip-close")
					div2.append(em).append(p);
				var div3 = $("<div/>").prop("class","infoTip-con")
					var h2 = $("<h2/>").html("安全巡查")
					var p = $("<p/>").html(list[i].content)
					div3.append(h2).append(p);
				div.append(div2).append(div3).appendTo($("#pInfoId"));
		}
		$(".infoTip-close").on("click",function(){//点击八叉消失
			this.parentNode.parentNode.style.display='none';
		});
	});
}

//学校端右上角消息提示
function schoolMessageUp(){
	var url = pname +"/ZeroReport/getSchoolMsg";
	$.post(url,function(res){
		$("#schoolWaitUpdateId").html(res.schoolWaitUpdateId);
		$("#schoolCompleteId").html(res.schoolCompleteId);
		$("#schoolNoReadId").html(res.schoolNoReadId);
		$("#schoolbohuiId").html(res.schoolbohuiId);
		$("#schoolbuluId").html(res.schoolbuluId);
		$("#loginTimeSpanId2").html(res.time.substring(0,19));
		var num = res.schoolWaitUpdateId+res.schoolCompleteId
		+res.schoolNoReadId+res.schoolbohuiId+res.schoolbuluId;
		if(num=="00000"){
			$("#classRedSpanId2").removeClass("layui-badge-dot")
		}else{
			$("#classRedSpanId2").addClass("layui-badge-dot")
		}
	});
	if(uDuty=='校长'){
		var url2 = pname +"/ZeroReport/getPatrolInfo";
		//校长加签下发和安全员巡查一个接口
		$.post(url2,function(res){
			var list = res.list;
			for(var i=0;i<list.length;i++){
				var div = $("<div/>").prop("class","infoTip")
					var div2 = $("<div/>").prop("class","infoTip-top")
						var em = $("<em/>").attr("class","icon-tip")
						var p = $("<i/>").attr("class","infoTip-close")
						div2.append(em).append(p);
					var div3 = $("<div/>").prop("class","infoTip-con")
						var h2 = $("<h2/>").html("校长加签下发")
						var p = $("<p/>").html(list[i].content)
						div3.append(h2).append(p);
					div.append(div2).append(div3).appendTo($("#pInfoId"));
			}
			$(".infoTip-close").on("click",function(){//点击八叉消失
				this.parentNode.parentNode.style.display='none';
			});
		});
	}
}

//学校零报告消息提示
function schoolMessage(){ 
	var url = pname +"/ZeroReport/checkAddTime";
	$.post(url,function(res){
		if(res.success){
			$("#infoTip").css('display','');	
			$("#tempTitleId").html(res.title);	
			$("#tempContentId").html(res.content);	
			$("#tempHrefId").prop("href",pname+res.url+"?status=gogo");	
			$("#infoMin").css('display','none');
		}else{
			$("#infoTip").css('display','none');
		}
	});
} 
//学校通知消息提示
function schoolNotify(){ 
	var url = pname +"/notify/checkNotify";
	$.post(url,function(res){
		if(res.success){
			$("#infoTip2").css('display','');	
			$("#tempTitleId2").html(res.title);	
			$("#tempContentId2").html("您有未读通知："+res.msg);	
			$("#tempHrefId2").prop("href",pname+"/notify/indexs");	
			$("#infoMin2").css('display','none');
		}else{
			//$("#infoTip2").css('display','none');
		}
	});
}
//教委右上角
function eduMessage(){
	var url = pname +"/ZeroReport/getEduMessage";
	$.post(url,function(res){
		$("#zeroNormalId").html(res.normal);
		$("#zeroUnNormalId").html(res.unnormal);
		$("#hasAttId").html(res.hasList);
		$("#nohasAttId").html(res.nohasList);
		$("#allTreviewListId").html(res.reviewList);
		$("#allTcheckListId").html(res.checkList);
		$("#loginTimeSpanId").html(res.time.substring(0,19));
		var num = res.normal+res.unnormal+res.hasList
		+res.nohasList+res.reviewList+res.checkList;
		if(num=="000000"){
			$("#classRedSpanId").removeClass("layui-badge-dot")
		}else{
			$("#classRedSpanId").addClass("layui-badge-dot")
		}
	});
}
/*公共对象，提供一些经常用到的方法，比如重置、刷新等操作*/
var toolsObject = {
		//重置方法
		resetGrid:function(type,node){
			if(type=="add"){
				//$("#add_update_panel select[name='type']").val("");
				//layui.form.render('select');	
				$("#add_update_panel select[name='floor']").val("");
				layui.form.render('select');	
				$("#add_update_panel select[name='tower']").val("");
				layui.form.render('select');	
				$("#add_update_panel select[name='area']").val("");
				layui.form.render('select');		
				$(".layui-form input[name='isspecial'][value='N']").next().find("i").click();
				$("#add_update_panel input[name='gridname").val("");	
				$("#add_update_panel input[name='gridpersion").val("");	
				$("#add_update_panel input[name='phone").val("");	
				$("#add_update_panel input[name='bak']").val("");	
			}else if(type=="all_query"){
				//$("#all_type").val("");
				$("#all_tower").val("");
				$("#all_floor").val("");
				$("#all_room").val("");
				$(".layui-form input[name='isspecial'][value='N']").next().find("i").click();
				layui.form.render('select');	
			}else if(type=="update_select"){
				  $("#update_panel select[name='edit_area']").val("");
				  $("#update_panel select[name='edit_floor']").val("");
				  $("#update_panel select[name='edit_tower']").val("");
				  $("#update_panel select[name='edit_room']").val("");
				  $("#update_panel input[name='name']").val("");
			}
		},
		format: function(value){
			return value;
		},
		//中文汉字转Unicode
		unicode:function(str){
	     	var value='';
	        for(var i = 0; i < str.length; i++) {
	            value += '\\u' + this.left_zero_4(parseInt(str.charCodeAt(i)).toString(16));
	        }
	        return value;
	    },
	    left_zero_4:function (str) {
	    	if(str != null && str != '' && str != 'undefined') {
	    		if(str.length == 2) {
	    			return '00' + str;
	    		}
	    	}
	    	return str;
	    },
	//刷新tabel
	refreshTable:function(data,id){
		layui.use(['table'], function(){
			var table = layui.table;
				table.reload(id, {
				page: {curr: 1}, //重新从第 1 页开始
    			where: data
  			});
		});
	}
};

//初始化左侧树
function CommonInitZStree(id){
	var zTree;
	var setting = {
			data : {   
				simpleData : {
				enable : true,    
			    idKey : "id",  
			    pIdKey : "pId",  
			    rootPId : null  
			    }
		    },
		    check: {
			autoCheckTrigger: true,   
			chkStyle: "checkbox",   
			chkboxType: { "Y": "s", "N": "s" }   
		    },
			callback: {
				onClick: zTreeOnClick
			}
	     };
	    var url = pname+'/Organization/getOrganizationForTable';
	            $.getJSON(url,function(result){
		                $.fn.zTree.init(
							$("#"+id),
							setting,
							result
						);
						$.fn.zTree.init(
								$("#hiddenTreeId"),
								setting,
								result
							);			
		
		             });
}
//直属单位左侧树
function CommonInitZStree2(id){
	var zTree;
	var setting = {
			data : {   
				simpleData : {
				enable : true,    
			    idKey : "id",  
			    pIdKey : "pId",  
			    rootPId : null  
			    }
		    },
		    check: {
			autoCheckTrigger: true,   
			chkStyle: "checkbox",   
			chkboxType: { "Y": "s", "N": "s" }   
		    },
			callback: {
				onClick: zTreeOnClick
			}
	     };
	    var url = pname+'/Organization/getOrganizationForTable2';
	            $.getJSON(url,function(result){
		                $.fn.zTree.init(
							$("#"+id),
							setting,
							result
						);
						$.fn.zTree.init(
								$("#hiddenTreeId"),
								setting,
								result
							);			
		
		             });
}
//获取当前日期
function getNowFormatDate() {
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = year + seperator1 + month + seperator1 + strDate;
    return currentdate;
}
//获取当前时间
function getNowFormatTime() {
    var date = new Date();
    var seperator2 = ":";
    var hours = date.getHours();
    var minutes = date.getMinutes()
    if (hours >= 1 && hours <= 9) {
    	hours = "0" + hours;
    }
    if (minutes >= 0 && minutes <= 9) {
    	minutes = "0" + minutes;
    }
    var currentdate = hours + seperator2 + minutes
            + seperator2 + date.getSeconds();
    return currentdate;
}
//左侧查询树
function queryTreeLeft(){
	var queryvalue = $("#queryTreeLeftId").val();
	if (queryvalue.length > 0) {
        var zTree = $.fn.zTree.getZTreeObj("hiddenTreeId");
        var nodeList = zTree.getNodesByParamFuzzy("name", queryvalue,0);
        //将找到的nodelist节点更新至Ztree内
        $.fn.zTree.init($("#typeTree"), zTree.setting, nodeList);
    } else {
    	CommonInitZStree('typeTree');                
    }
}
//关闭所有弹窗
function closeAllPage(){
	layer.closeAll();
}

/*****************************************/
//联合使用。数组中移除某一个元素
Array.prototype.indexOf = function (val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == val) return i;
    }
    return -1;
};

Array.prototype.remove = function (val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};

function getMaximin(arr,maximin) 
{ 
if(maximin=="max") 
{ 
return Math.max.apply(Math,arr); 
}
else if(maximin=="min") 
{ 
return Math.min.apply(Math, arr); 
} 
} 
/*var a=[3,2,4,2,10]; 
var b=[12,4,45,786,9,78]; 
console.log(getMaximin(a,"max"));//10
console.log(getMaximin(b,"min"));//04
*//*****************************************/

//导出PDF
function btnDownloadPageBypfd2(pdf_container){ //参数是'#pdf_container' 或 '.pdf_container',注意带前缀
	$(pdf_container).addClass('pdf'); //pdf的css在下一个代码中,作用是使得打印的内容能在pdf中完全显示
	var cntElem = g(pdf_container);
	var shareContent = cntElem; //需要截图的包裹的（原生的）DOM 对象
	var width = shareContent.offsetWidth; //获取dom 宽度
	var height = shareContent.offsetHeight; //获取dom 高度
	var canvas = document.createElement("canvas"); //创建一个canvas节点
	var scale = 2; //定义任意放大倍数 支持小数
	canvas.width = width * scale; //定义canvas 宽度 * 缩放，在此我是把canvas放大了2倍
	canvas.height = height * scale; //定义canvas高度 *缩放
	canvas.getContext("2d").scale(scale, scale); //获取context,设置scale 
	
	html2canvas(g(pdf_container), {
		allowTaint: true,
        taintTest: true,
        canvas: canvas,
		onrendered: function(canvas) {
		 	
		var context = canvas.getContext('2d');
		// 【重要】关闭抗锯齿
		context.mozImageSmoothingEnabled = false;
		context.webkitImageSmoothingEnabled = false;
		context.msImageSmoothingEnabled = false;
		context.imageSmoothingEnabled = false;
			 
		  var imgData = canvas.toDataURL('image/jpeg',1.0);//转化成base64格式,可上网了解此格式
		  var img = new Image();
		  img.src = imgData;
		  img.onload = function() {	
		    img.width = img.width/2;   //因为在上面放大了2倍，生成image之后要/2
		    img.height = img.height/2;
		    img.style.transform="scale(0.5)";
			console.log("img.width"+img.width);
			console.log("this.width="+this.width);
			console.log("this.height="+this.height);
		   /*if (this.width > this.height) {//此可以根据打印的大小进行自动调节
		    var doc = new jsPDF('l', 'mm', [this.width * 0.255, this.height * 0.225]);
		   } else {
		    var doc = new jsPDF('p', 'mm', [this.width * 0.255, this.height * 0.225]);
		   }
		   doc.addImage(imgData, 'jpeg', 10, 0, this.width * 0.225, this.height * 0.225);
		   doc.save('report_pdf_' + new Date().getTime() + '.pdf');*/
			
		    /****分页******/
		     var pageHeight = 841.89;//一页高度
		     var leftHeight = height * 0.75;//未打印内容高度
	      	 var position = 0;//页面偏移
	      	 var imgWidth = width;
	         //var imgHeight = 841.89;
	         var imgHeight =   height;
	         console.log("imgWidth="+imgWidth);
	         console.log("imgHeight="+imgHeight);
	      	  var doc = new jsPDF('p', 'pt', 'a4');
		     if(pageHeight >= leftHeight){//不需要分页，页面高度>=未打印内容高度
		     	console.log("不需要分页");
		     	 doc.addImage(imgData, 'jpeg', 35, 0, imgWidth*0.75, imgHeight*0.75);
		     }else{//需要分页
		     	console.log("需要分页");
		     	while(leftHeight>0){
		     	console.log("position="+position);
		     	console.log("leftHeight="+leftHeight);
		     	 doc.addImage(imgData, 'JPEG', 35, position, imgWidth*0.75, imgHeight*0.75);
		     	 leftHeight -= pageHeight; 
		     	 position -= 841.89; 
		     	 //避免添加空白页
		     	 if(leftHeight > 0){
		     	 	console.log("添加空白页");
		     	 	doc.addPage();
		     	 }
		     	}
		     }
		     var detailTitile = $("#detailTitileId").html();
		     doc.save(detailTitile + new Date().getTime() + '.pdf');//保存为pdf文件
		  }
		 },
		  background: "#fff", //一般把背景设置为白色，不然会出现图片外无内容的地方出现黑色，有时候还需要在CSS样式中设置div背景白色
		});
	$(pdf_container).removeClass('pdf');
}

//通用函数，主要用于获取dom元素
function g(selector){  
	    var method = selector.substr(0,1) == '.' ?  
	        'getElementsByClassName' : 'getElementById';  
	    return document[method](selector.substr(1));  
}

/** ***************** blockUI begin ********************* */

//调用 出现 遮罩层,显示 请等待;
function blockUI(msg){
	
	var blockMsg = "请等待";
	if(msg && msg!=''){
		blockMsg = msg;
	}
	
	$.blockUI({
	message:blockMsg,
	boxed:true,
	zIndex:9996
});
$("body").css("overflow","hidden");
};

/* 调用 使遮罩层消失;*/
function unBlockUI(){
	$.unblockUI();
$("body").css("overflow","auto");
}

/** ***************** blockUI end ********************* */

function resetPage(){
	var test = window.location.pathname;
	window.location.href=test;
	//window.location.reload();
}