package com.atguigu.springboot.platform.user.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.atguigu.springboot.common.CommonTool;
import com.atguigu.springboot.common.JdbcTemplateHandler;
import com.atguigu.springboot.common.Page;
import com.atguigu.springboot.entity.UserInfo;
import com.atguigu.springboot.platform.user.service.UserInfoService;
import com.atguigu.springboot.repository.UserInfoRepository;
  
  
@Service  
public class UserInfoServiceImpl implements UserInfoService{  
    @Resource  
    private UserInfoRepository userInfoRepository;  
  
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    JdbcTemplateHandler handler;
	
    @Transactional(readOnly=true)  
    @Override  
    public UserInfo findByUsername(String username) {  
        return userInfoRepository.findByUsername(username);  
    }
	@SuppressWarnings("rawtypes")
	@Override
	public Page getlist(int pageNum, int pageSize) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from user_info   ");
		Page pageData = null;
		try {
			pageData = handler.getPageData(sql.toString(), pageNum, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageData;
	}

	@SuppressWarnings("unused")
	@Transactional
	@Override
	public boolean registerData(String username, String password) {
	    // 生成uuid
        String id = CommonTool.getUUID();
        // 将用户名作为盐值
        ByteSource salt = ByteSource.Util.bytes(username);
        /*
        * MD5加密：
        * 使用SimpleHash类对原始密码进行加密。
        * 第一个参数代表使用MD5方式加密
        * 第二个参数为原始密码
        * 第三个参数为盐值，即用户名
        * 第四个参数为加密次数
        * 最后用toHex()方法将加密后的密码转成String
        * */
        String newPs = new SimpleHash("MD5", password, salt, 2).toHex();
        System.out.println(newPs);
        UserInfo userInfo = new UserInfo();
        userInfo.setName("一般管理员");
        userInfo.setPassword(newPs);
        userInfo.setSalt("");
        userInfo.setUsername(username);
        UserInfo save = userInfoRepository.save(userInfo);
        System.out.println(save);
		return true;
	}

	@Override
	public boolean addUser(UserInfo userInfo) {
		UserInfo save = userInfoRepository.save(userInfo);
		if (save!=null) {
			return true;
		}
		return false;
	}

	@Override
	public List<Map<String, Object>> getlist() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select * from user_info ");
		List<Map<String,Object>> queryForList = jdbcTemplate.queryForList(stringBuffer.toString());
		return queryForList;
	}  
  
}  