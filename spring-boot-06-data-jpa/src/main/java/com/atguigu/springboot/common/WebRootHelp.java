package com.atguigu.springboot.common;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

@Component
public class WebRootHelp {
	  public static String getWebRoot(HttpServletRequest request)
	  {
	    StringBuffer url = request.getRequestURL();
	    String webUrl = request.getServletContext().getContextPath() + "/";
	    return webUrl;
	  }

	  public static String getRemoteHost(HttpServletRequest request)
	  {
	    String ip = request.getHeader("x-forwarded-for");
	    if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip))) {
	      ip = request.getHeader("Proxy-Client-IP");
	    }
	    if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip))) {
	      ip = request.getHeader("WL-Proxy-Client-IP");
	    }
	    if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip))) {
	      ip = request.getRemoteAddr();
	    }
	    return ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
	  }

	  public static String getWebContext(HttpServletRequest request)
	  {
	    String webContext = request.getContextPath();
	    return webContext;
	  }

	  public static String getWebRealPath(HttpServletRequest request)
	  {
	    String webRealPath = request.getSession().getServletContext().getRealPath("");
	    return webRealPath;
	  }
	}
