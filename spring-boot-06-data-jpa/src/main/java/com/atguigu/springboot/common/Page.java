package com.atguigu.springboot.common;

import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class Page<T>  extends PageImpl<T> {
	private static final long serialVersionUID = 1L;
	  private Integer liststep;
	  private Integer listbegin;
	  private Integer listend;
	  private Integer currentPage;
	  private Integer totalPage;
	  private Integer total;
	  
	  public Page(List<T> content)
	  {
	    super(content);
	    this.liststep = Integer.valueOf(5);
	  }

	  public Page(List<T> content, Pageable pagealbe, Long total) {
	    super(content, pagealbe, total.longValue());
	    this.liststep = Integer.valueOf(5);
	  }

	  public Integer getListstep() {
	    return this.liststep;
	  }

	  public Integer getListbegin() {
	    int currentPage = getNumber() + 1;
	    this.listbegin = Integer.valueOf(currentPage - (int)Math.ceil(this.liststep.intValue() / 2.0D));
	    if (this.listbegin.intValue() < 1)
	    {
	      this.listbegin = Integer.valueOf(1);
	    }
	    return this.listbegin;
	  }

	  public Integer getListend() {
	    int currentPage = getNumber() + 1;
	    int totalPage = getTotalPages();
	    this.listend = Integer.valueOf(currentPage + this.liststep.intValue() / 2);
	    if (this.listend.intValue() > totalPage)
	    {
	      this.listend = Integer.valueOf(totalPage + 1);
	    }
	    return this.listend;
	  }

	  public Integer getCurrentPage() {
	    this.currentPage = Integer.valueOf(getNumber() + 1);
	    return this.currentPage;
	  }

	  public Integer getTotalPage() {
	    this.totalPage = Integer.valueOf(getTotalPages());
	    return this.totalPage;
	  }

	  public void setListstep(Integer liststep) {
	    this.liststep = liststep;
	  }

	  public void setListbegin(Integer listbegin) {
	    this.listbegin = listbegin;
	  }

	  public void setListend(Integer listend) {
	    this.listend = listend;
	  }

	  public void setCurrentPage(Integer currentPage) {
	    this.currentPage = currentPage;
	  }

	  public void setTotalPage(Integer totalPage) {
	    this.totalPage = totalPage;
	  }

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}
	  
	  
	  
	  
}
