package com.atguigu.springboot.common;

public class Constants {
    /** 默认页码 1 */
    public static final String PAGE_NUM = "1";
    /** 默认行数 10 */
    public static final String PAGE_SIZE = "10";
}
