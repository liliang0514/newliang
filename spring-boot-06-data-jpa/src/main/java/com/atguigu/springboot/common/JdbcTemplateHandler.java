package com.atguigu.springboot.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class JdbcTemplateHandler {

	  @Autowired
	  private JdbcTemplate jdbcTemplate;
	
	  public JdbcTemplate getJdbcTemplate()
	  {
	    return this.jdbcTemplate;
	  }
	
	  @SuppressWarnings({ "rawtypes", "unchecked" })
	  @Transactional(readOnly=true)
	  public Page getPageData(String sql, int pageNo, int pageSize)
	    throws Exception
	  {
	    String countSql = "select count(*) from (" + sql + ") cnt";
	    Long totalCount = (Long)this.jdbcTemplate.queryForObject(countSql, Long.class);
	
	    String pageSql = sql + " limit " + (pageNo - 1) * pageSize + "," + pageSize;
	    List resultMapList = this.jdbcTemplate.queryForList(pageSql);
	
	  if ((pageNo != 1) && (resultMapList.isEmpty())) {
	     pageNo = 1;
	     String tmpPageSql = sql + " limit " + (pageNo - 1) * pageSize + "," + pageSize;
	      resultMapList = this.jdbcTemplate.queryForList(tmpPageSql);
	    }
	
	      PageRequest pageRequest = buildPageRequest(pageNo, pageSize);
	      Page page = new Page(resultMapList, pageRequest, totalCount);
	
	    return page;
	  }
	
	  @Transactional(readOnly=true)
	  public Page getPageDataNoHomePage(String sql, int pageNo, int pageSize)
	    throws Exception
	  {
	    String countSql = "select count(*) from (" + sql + ") cnt";
	    Long totalCount = (Long)this.jdbcTemplate.queryForObject(countSql, Long.class);
	
	    String pageSql = sql + " limit " + (pageNo - 1) * pageSize + "," + pageSize;
	    List resultMapList = this.jdbcTemplate.queryForList(pageSql);
	
	    PageRequest pageRequest = buildPageRequest(pageNo, pageSize);
	    Page page = new Page(resultMapList, pageRequest, totalCount);
	
	    return page;
	  }
	
	  @Transactional(readOnly=true)
	  public Page getPageDataByOracle(String sql, int pageNo, int pageSize)
	    throws Exception
	  {
	    String countSql = "select count(*) from (" + sql + ") cnt";
	Long totalCount = (Long)this.jdbcTemplate.queryForObject(countSql, Long.class);
	
	if (pageNo <= 0) {
	  pageNo = 1;
	}
	if ((pageSize < 1) || (pageSize > 100)) {
	  pageSize = 10;
	}
	int startrow = (pageNo - 1) * pageSize;
	
	String runsql = "select * from (select a.*, rownum r from (" + sql + ") a where rownum <= " + (startrow + pageSize) + ") where r > " + startrow;
	
	    List maps = new ArrayList();
	    maps = this.jdbcTemplate.queryForList(runsql);
	
	    PageRequest pageRequest = buildPageRequest(pageNo, pageSize);
	    Page pageView = new Page(maps, pageRequest, totalCount);
	
	    return pageView;
	  }
	
	  @Transactional(readOnly=true)
	  private PageRequest buildPageRequest(int pageNumber, int pagzSize)
	  {
	    return new PageRequest(pageNumber - 1, pagzSize, null);
	  }
	
	  @Transactional(readOnly=true)
	  public List<Map<String, Object>> queryForMapList(String sql)
	    throws Exception
	  {
	    List list = new ArrayList();
	
	    list = this.jdbcTemplate.queryForList(sql);
	    return list;
	  }
	
	  @Transactional(readOnly=true)
	  public Object queryForObject(String sql)
	    throws Exception
	  {
	    Object obj = this.jdbcTemplate.queryForObject(sql, Object.class);
	    return obj;
	  }
	  public int updateForSql(String sql) throws Exception {
	    int num = this.jdbcTemplate.update(sql);
	    return num;
	  }
	
	  public Map<String, Object> queryForMap(String sql)
	    throws Exception
	  {
	    Map map = this.jdbcTemplate.queryForMap(sql);
	    return map;
	  }
	
	  public List<String> queryForList(String sql)
	    throws Exception
	  {
	    List list = this.jdbcTemplate.queryForList(sql, String.class);
	    return list;
	  }
	
	  public int[] batchUpdate(String sql, List<Object[]> batchArgs)
	    throws Exception
	  {
	    return this.jdbcTemplate.batchUpdate(sql, batchArgs);
	  }
	
	  public void execute(String sql)
	    throws Exception
	  {
	    this.jdbcTemplate.execute(sql);
	  }
}
