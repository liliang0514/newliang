package com.atguigu.springboot.common;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class PageInfo {
	  private static final int PAGESIZE = 10;
	  private int total;
	  private List rows;

	  @JsonIgnore
	  private int from;

	  @JsonIgnore
	  private int size;

	  @JsonIgnore
	  private int nowpage;

	  @JsonIgnore
	  private int pagesize;

	  @JsonIgnore
	  private Map<String, Object> condition;

	  @JsonIgnore
	  private String sort = "seq";

	  @JsonIgnore
	  private String order = "asc";

	  public PageInfo()
	  {
	  }

	  public PageInfo(int nowpage, int pagesize)
	  {
	    if (nowpage < 0) {
	      this.nowpage = 1;
	    }
	    else {
	      this.nowpage = nowpage;
	    }

	    if (pagesize < 0)
	      this.pagesize = 10;
	    else {
	      this.pagesize = pagesize;
	    }

	    this.from = ((this.nowpage - 1) * this.pagesize);
	    this.size = this.pagesize;
	  }

	  public PageInfo(int nowpage, int pagesize, String sort, String order)
	  {
	    if (nowpage < 0) {
	      this.nowpage = 1;
	    }
	    else {
	      this.nowpage = nowpage;
	    }

	    if (pagesize < 0)
	      this.pagesize = 10;
	    else {
	      this.pagesize = pagesize;
	    }

	    this.from = ((this.nowpage - 1) * this.pagesize);
	    this.size = this.pagesize;

	    this.sort = sort;
	    this.order = order;
	  }

	  public int getTotal() {
	    return this.total;
	  }

	  public void setTotal(int total) {
	    this.total = total;
	  }

	  public List getRows()
	  {
	    return this.rows;
	  }

	  public void setRows(List rows)
	  {
	    this.rows = rows;
	  }

	  public int getFrom() {
	    return this.from;
	  }

	  public void setFrom(int from) {
	    this.from = from;
	  }

	  public int getSize() {
	    return this.size;
	  }

	  public void setSize(int size) {
	    this.size = size;
	  }

	  public int getNowpage() {
	    return this.nowpage;
	  }

	  public void setNowpage(int nowpage) {
	    this.nowpage = nowpage;
	  }

	  public int getPagesize() {
	    return this.pagesize;
	  }

	  public void setPagesize(int pagesize) {
	    this.pagesize = pagesize;
	  }

	  public Map<String, Object> getCondition() {
	    return this.condition;
	  }

	  public void setCondition(Map<String, Object> condition) {
	    this.condition = condition;
	  }

	  public String getSort() {
	    return this.sort;
	  }

	  public void setSort(String sort) {
	    this.sort = sort;
	  }

	  public String getOrder() {
	    return this.order;
	  }

	  public void setOrder(String order) {
	    this.order = order;
	  }
	}