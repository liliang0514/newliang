/**
 * 2019年1月18日上午10:54:35
 * Tianzhi.java
 * liliang
 */
package com.atguigu.springboot.config;

/**
 * @author Administrator
 *
 */
public enum Tianzhi {
	PAGE_NUM("1"),PAGE_SIZE("10"),NUM(23);
    private String A;
    private int B;
	private Tianzhi(String A){
        this.setA(A);
    }
	public String getA() {
		return A;
	}
	public void setA(String a) {
		A = a;
	}
	private Tianzhi(int B){
		this.setB(B);
	}
	public int getB() {
		return B;
	}
	public void setB(int b) {
		B = b;
	}
}
